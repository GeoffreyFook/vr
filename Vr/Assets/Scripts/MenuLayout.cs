﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton
{

    public string Text { get; set;}
    public EnumMenuButton EnumButton { get; set; }
    public List<MenuButton> SubButtons { get; set; }
}

public enum EnumMenuButton
{
    CreateNewScene = 1,
    LoadFile = 2,
    ToolsMenu = 3,
    TMMoveToViewPoint = 4,
    TMAddViewPoint = 5,
    TMSelectByClicking = 6,
    TMSaveScene = 7,
    Back = 8
}

