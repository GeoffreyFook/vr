﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    List<MenuButton> root;
    public delegate void dgMenuButtons(EnumMenuButton menuButton);
    public GameObject myButton;
    GameObject myButtonClone;
    List<GameObject> buttonClonesToDestroy;
    public GameObject canvas;

    // Use this for initialization
    void Start ()
    {
        canvas = GameObject.Find("Canvas");
        buttonClonesToDestroy = new List<GameObject>();
        root = InitalizeRoot();
        SetButtons();
    }

    private List<MenuButton> InitalizeRoot()
    {
        return root = new List<MenuButton>
        {
            new MenuButton
            {
                Text = "Test Button",
                EnumButton = EnumMenuButton.CreateNewScene
            },
            new MenuButton
            {
                Text = "Test Button2",
                EnumButton = EnumMenuButton.LoadFile,
                SubButtons = new List<MenuButton>
                {
                    new MenuButton
                    {
                        Text = "Sub 1",
                        EnumButton = EnumMenuButton.ToolsMenu,
                        SubButtons = new List<MenuButton>
                        {
                            new MenuButton
                            {
                                Text = "Move to view Point",

                                EnumButton = EnumMenuButton.TMMoveToViewPoint
                            }
                        }
                    },
                     new MenuButton
                    {
                        Text = "Sub 2",
                        EnumButton = EnumMenuButton.TMSelectByClicking
                    }
                }
            },
            new MenuButton
            {
                Text = "Test Button3",
                EnumButton = EnumMenuButton.TMAddViewPoint
            }
        };
    }

    private void SetButtons()
    {
        var positonY = 0;
    
        foreach (var menuButton in root)
        {


        }
    }

    private void CreateButton(EnumMenuButton enumMenutItem, List<MenuButton> menuButtons, int posY, string text)
    {
        var transfrom = new Vector3(canvas.transform.position.x - 115, canvas.transform.position.y + 100 - posY, canvas.transform.position.z);

        myButtonClone = Instantiate(myButton, transfrom, Quaternion.identity);
        myButtonClone.transform.parent = canvas.transform;
        myButtonClone.transform.Find("Text").GetComponent<Text>().text = text;

        var btn = myButtonClone.GetComponent<UnityEngine.UI.Button>();
        btn.onClick.AddListener(() => TaskOnClick(menuButtons, enumMenutItem));
        buttonClonesToDestroy.Add(myButtonClone);

        posY += 23;
    }

    void TaskOnClick(List<MenuButton> menuButton, EnumMenuButton enumMenuButton)
    {
        if (menuButton == null)
        {
            ProcessButton(enumMenuButton);

        }
        else
        {
            foreach (var button in buttonClonesToDestroy)
                Destroy(button);

            root = enumMenuButton == EnumMenuButton.Back ? InitalizeRoot() : menuButton;
            SetButtons();
        }

        Debug.Log("You have clicked the button!");
    }

    public void ProcessButton(EnumMenuButton menuButton)
    {
        switch (menuButton)
        {
            case EnumMenuButton.CreateNewScene:
                //OnMultipleOfFiveReached();
                print("It Worked");
                break;
            case EnumMenuButton.LoadFile:
                break;
            case EnumMenuButton.TMAddViewPoint:
                break;
            case EnumMenuButton.TMMoveToViewPoint:
                print("Moving Worked Worked");
                break;
        }

    }


    public class ProcessButtons
    {
        public delegate void dgEventRaiser();
        public event dgEventRaiser OnMultipleOfFiveReached;

        public void Process(EnumMenuButton menuButton)
        {
            switch(menuButton)
            {
                case EnumMenuButton.CreateNewScene:
                    OnMultipleOfFiveReached();
                    break;
                case EnumMenuButton.LoadFile:
                    break;
                case EnumMenuButton.TMAddViewPoint:
                    break;
            }

        }
    }

    void Update ()
    {
		
	}

    void FixedUpdate()
    {

    }
}

